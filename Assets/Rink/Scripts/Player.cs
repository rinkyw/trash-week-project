using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    // Variables
    private Crops CropInRange;

    // Inventory system
    [HideInInspector] public int amountOfCarrots, amountOfCorn, amountOfEggplants, amountOfPumpkins, amountOfTomatoes, amountOfTurnips;
    [HideInInspector] public int amountOfCarrotsSeeds, amountOfCornSeeds, amountOfEggplantsSeeds, amountOfPumpkinsSeeds, amountOfTomatoesSeeds, amountOfTurnipsSeeds;
    //

    void Start()
    {

    }
    
    void Update()
    {
        if(CropInRange != null)
        {
            // if there is a crop in range make sure you can harvest that shit here
        }
    }

    private void OnTriggerEnter(Collider collider)
    {
        if(collider.name == "Dirt_Pile")
        {
            CropInRange = collider.GetComponent<Crops>();

            Debug.Log("Hit: " + collider.name);
        }
    }

    private void OnTriggerExit(Collider collider)
    {
        if (collider.name == "Dirt_Pile")
        {
            CropInRange = null;
        }
    }
}

