﻿namespace EasyScripts
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEditor;
    // Extra Namespaces
    using System.IO;
    using System.Linq;

    [InitializeOnLoad, ExecuteInEditMode]
    public class ScriptMaker : EditorWindow
    {
        // Variables
        private string ProjectPath;
        private string DefaultTemplatePath;
        private string TemplatePath;

        ///<summary>The location this script will be stored at.</summary>
        private string ScriptPath;
        ///<summary>The Script and Class name are the same.</summary>
        private string ClassName;

        // Keep track of the templates
        private string[] TemplateSelected;
        private int TemplateNumberSelected;
        private List<FileInfo> ScriptTemplateList;

        [MenuItem("Window/Easy Scripts Window")]
        static void easyScriptsWindow()
        {
            GetWindow<ScriptMaker>("Easy Scripts");
        }

        private void Start()
        {
            Beginning();
        }

        private void Beginning()
        {
            // Check if the ProjectPath is null, if so then set the project path to its correct path on the drive.
            if (ProjectPath == null)
            {
                ProjectPath = Application.dataPath;
            }

            // Check if the template path exists in the user's documents folder, if not create the directory
            CheckDocumentsTemplatePath(System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments) + "/EasyScripts Templates");

            // Set TemplatePath to the directory in the documents folder with all the templates
            TemplatePath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments) + "/EasyScripts Templates";
            
            // Check if DefaultTemplatePath is null, if so then set it to the correct path.
            if (DefaultTemplatePath == null)
            {
                DefaultTemplatePath = ProjectPath + "/EasyScripts/Templates";
            }
            // Check if the ScriptPath is null, if so then set it to its default value of /Scripts.
            if (ScriptPath == null)
            {
                // Set default script path
                ScriptPath = ProjectPath + "/Scripts";
            }
            // Check if ScriptTemplateList is null, if so then initialize a new List.
            if (ScriptTemplateList == null)
            {
                ScriptTemplateList = new List<FileInfo>();
            }

            // Get the default values for the paths
            try
            {
                GetDefaultValues();
            }
            catch { SetDefaultValues(); }

            // Check for the files in the directory of TemplatePath
            GetFilesInDirectory(TemplatePath);
        }

        private void OnGUI()
        {
            // Execute all the code that needs to be done before the Easy Scripts can properly function,
            // This is going to repeat getting executed to make sure everything is always up to date.
            Beginning();

            GUILayout.Space(5);

            // Path where the file will be stored
            GUILayout.Label(new GUIContent(ProjectPath + "/", "This is the project path up until the Assets folder from the project, " +
                "put in the textfield something like: Scripts or Scripts/MoreScripts."));

            GUILayout.Space(1);

            // Selection window for where the user wants to store their scripts.
            if (GUILayout.Button("Select Script Folder"))
            {
                ScriptPath = EditorUtility.SaveFolderPanel("Save scripts at", ProjectPath, "");
                SetDefaultValues();
            }
            // Show the user where their scripts are going to be saved.

            GUILayout.Space(7);

            GUILayout.Label("ScriptPath:");
            GUILayout.Label(ScriptPath);

            GUILayout.Space(7);

            // Set the name of the Class and the Filename. e.g. Player (Player.cs, class Player)
            GUILayout.Label("Class/File name");
            ClassName = EditorGUILayout.TextField(new GUIContent(".cs", "Put the name of the class and script here."), ClassName);

            GUILayout.Space(7);

            TemplateSelected = null;
            TemplateSelected = ScriptTemplateList.Select(I => I.Name).ToArray();
            // Drop down menu.
            TemplateNumberSelected = EditorGUILayout.Popup("Select Template", TemplateNumberSelected, TemplateSelected, EditorStyles.popup);

            GUILayout.Space(7);

            // Button to make the script
            if (GUILayout.Button("Create Script From Preset."))
            {
                if (TemplateSelected != null)
                {
                    if (!string.IsNullOrEmpty(ScriptPath) && !string.IsNullOrWhiteSpace(ScriptPath))
                    {
                        if (!string.IsNullOrEmpty(ClassName) && !string.IsNullOrWhiteSpace(ClassName))
                        {
                            // Creating the .cs file
                            string FullFilePathAndName = ScriptPath + "/" + ClassName + ".cs";
                            using (var writer = new System.IO.StreamWriter(FullFilePathAndName))
                            {
                                // 
                                writer.WriteLine(GetScriptTemplateSetClassName(TemplatePath + "/" + TemplateSelected[TemplateNumberSelected], ClassName));
                            }
                        }
                        else { Debug.LogError("Error with ClassName: " + ClassName); }
                    }
                    else { Debug.LogError("Error with ScriptStoringPath: " + ScriptPath); }
                }
                else
                {
                    GetFilesInDirectory(TemplatePath);
                    Debug.Log("Completed Looking for Templates. Found " + ScriptTemplateList.ToArray().Length);
                }
            }

            GUILayout.Space(14);

            if (GUILayout.Button("Create New Template"))
            {
                // Create the new template with a (In most cases) proper name.
                CreateTemplateFile(TemplatePath, "csScriptTemplate_" + (ScriptTemplateList.ToArray().Length + 1) + ".txt");

                // Open the folder with the new template created.
                EditorUtility.RevealInFinder(System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments) + 
                    "/EasyScripts Templates/" + "csScriptTemplate_" + (ScriptTemplateList.ToArray().Length + 1) + ".txt");
            }

            GUILayout.Space(5);

            if (GUILayout.Button("Go To Preset Folder"))
            {
                EditorUtility.RevealInFinder(System.Environment.GetFolderPath(
                    System.Environment.SpecialFolder.MyDocuments) + "/EasyScripts Templates");
            }
        }
        #region Custom Functions

        /// <summary>Get the paths of the values in the settings.txt file.</summary>
        private void GetDefaultValues()
        {
            // Make an array which seperates all the variables because of ; stored in the settings.txt file
            string values = GetContentFromFileAtPath(DefaultTemplatePath + "/settings.txt");

            string[] PathString = values.Split(';');
            List<string> valuesList = new List<string>();

            for (int i = 0; i < PathString.Length; i++)
            {
                // add the good values and string parts to the list.
                if (!string.IsNullOrEmpty(PathString[i]) && !string.IsNullOrWhiteSpace(PathString[i]))
                {
                    valuesList.Add(PathString[i]);
                }
            }

            // Set the values to the correct variables
            for (int i = 0; i < valuesList.ToArray().Length; i++)
            {
                string[] CurrentValue = valuesList[i].Split('=');
                switch (CurrentValue[0])
                {
                    case "DefaultTemplatePath":
                        break;
                    case "TemplatePath":
                        TemplatePath = CurrentValue[1];
                        break;
                    case "ScriptPath":
                        ScriptPath = CurrentValue[1];
                        break;
                }
            }
        }
        /// <summary>Set the paths of the values in the settings.txt file.</summary>
        private void SetDefaultValues()
        {
            // The text that should be inside the settings.txt file.
            string settingsText = "DefaultTemplatePath=" + DefaultTemplatePath + ";TemplatePath=" + TemplatePath + ";ScriptPath="+ ScriptPath + ";";
            // Create the settings.txt file with the paths as strings
            CreateAndWriteFile(DefaultTemplatePath + "/settings.txt", settingsText);
        }

        /// <summary>Check if the Directory exist for the templates. If not then make the folder directory.</summary>
        /// <param name="FilePath">The Path of the file directory where all the templates will be stored</param>
        private void CheckDocumentsTemplatePath(string FilePath)
        {
            if (!System.IO.Directory.Exists(FilePath))
            {
                System.IO.Directory.CreateDirectory(FilePath);
            }
        }

        /// <summary>Create a file and write all the text that needs to be in the file</summary>
        /// <param name="NewFilePath">The path of the file the user wants to write to</param>
        /// <param name="WriteText">The text the user wants inside of the file</param>
        private void CreateAndWriteFile(string NewFilePath, string WriteText)
        {
            using (var writer = new System.IO.StreamWriter(NewFilePath))
            {
                writer.WriteLine(WriteText);
            }
        }

        /// <summary>Check all the files in the Template path, if there are no templates to choose from then make a default template.</summary>
        /// <param name="Path">File Path for the template folder</param>
        private void GetFilesInDirectory(string Path)
        {
            // Clear Script Template List, if this can't occur then nothing will happen.
            try
            {
                // Clear the List so there wont be duplicate templates in the appearing in the selection drop down menu.
                ScriptTemplateList.Clear();
            }
            catch { }

            // Go though all the *.txt files in the desired directory and add them to the List<FileInfo>.
            DirectoryInfo dir = new DirectoryInfo(Path);
            FileInfo[] ScriptTemplates = dir.GetFiles("*.txt");
            // Go through the array of templates and put the in the list.
            if (ScriptTemplates.Length > 0)
            {
                // Go through the Array and add the templates to the Global List in the correct order.
                for (int i = 0; i < ScriptTemplates.Length; i++)
                {
                    try {
                        ScriptTemplateList.Add(ScriptTemplates[i]);
                    }
                    catch { //Debug.Log("Couldn't add Template " + i + " to the list!");
                    }
                }
            }
            else
            {
                // Create a default template if no compatible templates exist.
                CreateTemplateFile(TemplatePath, "csScriptTemplate_1.txt");
            }
        }

        /// <summary>Create a new template at the desired path.</summary>
        /// <param name="newFilePath">The path where the new file should be stored.</param>
        /// <param name="FileName">The name the new template should have (.txt is not needed).</param>
        private void CreateTemplateFile(string newFilePath, string FileName)
        {
            // Create the default template in the template folder.
            CreateAndWriteFile(newFilePath + "/" + FileName,
                GetContentFromFileAtPath(ProjectPath + "/EasyScripts/Templates" + "/Default_cs_ScriptTemplate.txt"));

            //DefaultTemplatePath = ProjectPath + "/EasyScripts/Templates";
        }

        /// <summary>Take the text from the user defined template and replace the #CLASSNAME# with the class name the user has given.</summary>
        /// <param name="FilePath">The path where the script should be stored.</param>
        /// <param name="DesiredClassName">The class name the user has given.</param>
        /// <returns>The correct version of the template with the desired class name</returns>
        private string GetScriptTemplateSetClassName(string FilePath, string DesiredClassName)
        {
            return System.IO.File.ReadAllText(FilePath).Replace("#CLASSNAME#", DesiredClassName);
        }

        /// <summary>Get and return the text from the desired file</summary>
        /// <param name="FilePath">The path to the desired file.</param>
        /// <returns>The text from the desired file.</returns>
        private string GetContentFromFileAtPath(string FilePath)
        {
            return System.IO.File.ReadAllText(FilePath);
        }

        #endregion
    }
}
