using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CropType
{
    AFK = 0,
    Carrot, Corn, Eggplant,
    Pumpkin, Tomato, TurnIp = 6
}

public class Crops : MonoBehaviour
{
    // Variables
    [SerializeField] private float maxGrowthTime;

    [Header("Any objects and Attributes that have something to do with models and 3D assets.")]
    [SerializeField] private CropType cropType;
    [SerializeField] private GameObject CurrentCropModdel;

    // private variables
    private float TimeOfPlanting;
    private GlobalGameManager globalGameManager;
    
    [HideInInspector] public bool isReadyForHarvest = true;

    void Start()
    {
        globalGameManager = GameObject.Find("Global Game Manager").GetComponent<GlobalGameManager>();
        maxGrowthTime = GameObject.Find("Global Game Manager").GetComponent<GlobalGameManager>().GrowthTimer;

        NewPlant();
    }
    
    void Update()
    {
        if (TimeOfPlanting > maxGrowthTime)
        {
            isReadyForHarvest = true;
        }
        else
        {
            TimeOfPlanting += Time.deltaTime;
        }


        try
        {
            // Get the scale the fruit needs to be
            float FruitScale = TimeOfPlanting / maxGrowthTime;
            // Set the scale of the fruit.
            CurrentCropModdel.transform.localScale = new Vector3(FruitScale, FruitScale, FruitScale);
        } catch { Debug.Log("An unknown error prevented this code from working!"); }
    }

    private void NewPlant()
    {
        switch (cropType)
        {
            case CropType.AFK:
                CurrentCropModdel = null;
                break;
            case CropType.Carrot:
                CurrentCropModdel = Instantiate(globalGameManager.PlantPrefabs[0], transform);
                break;
            case CropType.Corn:
                CurrentCropModdel = Instantiate(globalGameManager.PlantPrefabs[1], transform);
                break;
            case CropType.Eggplant:
                CurrentCropModdel = Instantiate(globalGameManager.PlantPrefabs[2], transform);
                break;
            case CropType.Pumpkin:
                CurrentCropModdel = Instantiate(globalGameManager.PlantPrefabs[3], transform);
                break;
            case CropType.Tomato:
                CurrentCropModdel = Instantiate(globalGameManager.PlantPrefabs[4], transform);
                break;
            case CropType.TurnIp:
                CurrentCropModdel = Instantiate(globalGameManager.PlantPrefabs[5], transform);
                break;
        }

        // Set the shit to 0 bitch
        CurrentCropModdel.transform.localScale = new Vector3(0, 0, 0);
    }
}